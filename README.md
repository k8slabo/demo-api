# demo-api

Le but de cet exercice est de mettre en place un pipeline d'intégration et de déploiement continu. La partie déploiement sera basée sur l'approche GitOps: un processus tournant à l'intérieur du cluster sera chargé de détecter les modifications effectuées dans un repository Git et, en fonction de celles-ci, mettra l'application à jour.
Pour ce faire, vous allez effectuer les actions suivantes:
- coder un serveur web très simple
- créer un projet GitLab pour gérer les sources
- mettre en place un cluster Kubernetes basé sur k3s
- mettre en place un pipeline d'intégration continu avec GitLab CI
- mettre en place un pipeline de déploiement continu en utilisant ArgoCD
- test de la chaîne complète

Le but de l'ensemble de ces actions étant qu'une modification envoyée dans le projet GitLab déclenche automatiquement les tests et le déploiement de la nouvelle version du code sur le cluster Kubernetes.
